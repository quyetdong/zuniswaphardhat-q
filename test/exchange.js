const { expect } = require("chai");
const { ethers } = require("hardhat");

const toWei = (value) => ethers.utils.parseEther(value.toString());
const fromWei = (value) =>
  ethers.utils.formatEther(
    typeof value === "string" ? value : value.toString()
  );
const getBalance = ethers.provider.getBalance;

describe.skip("Greeter", function () {
  it("Should return the new greeting once it's changed", async function () {
    const Greeter = await ethers.getContractFactory("Greeter");
    const greeter = await Greeter.deploy("Hello, world!");
    await greeter.deployed();

    expect(await greeter.greet()).to.equal("Hello, world!");

    const setGreetingTx = await greeter.setGreeting("Hola, mundo!");

    // wait until the transaction is mined
    await setGreetingTx.wait();

    expect(await greeter.greet()).to.equal("Hola, mundo!");
  });
});

describe("Exchange", async () => {
  let token;
  let exchange;
  let owner;
  let user;

  beforeEach(async () => {
    [owner, user] = await ethers.getSigners();

    const Token = await ethers.getContractFactory("Token");
    token = await Token.deploy("new token", "TK", toWei(1000000));
    await token.deployed();

    const Exchange = await ethers.getContractFactory("Exchange");
    exchange = await Exchange.deploy(token.address);
    await exchange.deployed();
  });

  describe.skip("addLiquidity", async () => {
    it("adds liquidity", async () => {
      await token.approve(exchange.address, toWei(200));

      console.log("* 1 token addr ", token.address);
      console.log("* 1 exchange addr ", exchange.address);

      await exchange.addLiquidity(toWei(200), { value: toWei(100) });

      expect(await getBalance(exchange.address)).to.equal(toWei(100));
      expect(await exchange.getReserve()).to.equal(toWei(200));
    });
  });

  describe.skip("getPrice", async () => {
    it("returns correct prices", async () => {
      await token.approve(exchange.address, toWei(2000));
      await exchange.addLiquidity(toWei(2000), { value: toWei(1000) });

      const tokenReserve = await exchange.getReserve();
      const etherReserve = await getBalance(exchange.address);

      // 1/1000 ETH per token
      expect(await exchange.getPrice(etherReserve, tokenReserve)).to.eq(500);

      // 1/1000 token per ETH
      expect(await exchange.getPrice(tokenReserve, etherReserve)).to.eq(2000);

    });
  });

  describe.skip("getTokenAmount", async () => {
    it("returns correct token amount", async () => {
      await token.approve(exchange.address, toWei(2000));
      await exchange.addLiquidity(toWei(2000), { value: toWei(1000) });

      // const tokenReserve = await exchange.getReserve();
      // const etherReserve = await getBalance(exchange.address);

      // get token amount 
      // console.log("towei 1", toWei(1));
      // console.log("tokenReserve", await exchange.getReserve());


      let tokensOut = await exchange.getTokenAmount(toWei(1));
      expect(fromWei(tokensOut)).to.equal("1.978041738678708079");

      tokensOut = await exchange.getTokenAmount(toWei(100));
      expect(fromWei(tokensOut)).to.equal("180.1637852593266606");

      tokensOut = await exchange.getTokenAmount(toWei(1000));
      expect(fromWei(tokensOut)).to.equal("994.974874371859296482");

    });
  });

  describe("getEtherAmount", async () => {
    it("returns correct ether amount", async () => {
      await token.approve(exchange.address, toWei(2000));
      await exchange.addLiquidity(toWei(2000), { value: toWei(1000) });

      let ethersOut = await exchange.getEtherAmount(toWei(1));
      expect(fromWei(ethersOut)).to.equal("0.494755096227367453");

      ethersOut = await exchange.getEtherAmount(toWei(10));

      console.log("ethers out ", ethersOut);
      // console.log("ethers out", fromWei(ethersOut));

      expect(fromWei(ethersOut)).to.equal("4.925618189959699487");

    });
  });

  describe("full cycle", async () => {
    it("returns correct result", async () => {
      // add liquidity 100 ethers, 200 tokens
      await token.approve(exchange.address, toWei(200));

      const balanceBeforeAdd = await getBalance(owner.address);
      const liquid = await exchange.addLiquidity(toWei(200), { value: toWei(100) });
      // console.log("add liquid ", liquid);
      const fee = balanceBeforeAdd.sub(await getBalance(owner.address)).sub(toWei(100));
      // console.log("add liquid fee", fee);
        
      const ethBeforeRemove = await getBalance(owner.address);
      console.log("ether before ", fromWei(ethBeforeRemove));

      // get user balance
      // const userBalance = await getBalance(user.address);
      // console.log(userBalance);

      // user swapp ether to token, expect at least 18 tokens for 10 ethers
      await exchange.connect(user).ethToTokenSwap(toWei(1.8), { value: toWei(1) });
  
      // liquidity provider remove liquidity
      const removeLiquid = await exchange.removeLiquidity(toWei(100));
      // console.log("remove liquid ", removeLiquid);

      const ethAfterRemove = await getBalance(owner.address);
      // console.log("ether get back ", fromWei(ethAfterRemove.sub(ethBeforeRemove))); // 101 - gas fee
      // console.log("ether get back ", 101 - fromWei(fee)); // 101 - gas fee         
      
      expect(await exchange.getReserve()).to.equal(toWei(0));
      expect(await getBalance(exchange.address)).to.equal(toWei(0));
    });

  });

});

